# m2dl-mobe-coordinature

Blondel Richard, Lépiné Axel et Libet Guillaume

# les infos nécessaires pour lancer l'application
    - Il suffit juste de pull le code sur le lien git et lancer l'application, ou décompresser
    l'archive et l'ouvrir dans Android Studio.
    - Nous vous conseillons de jouer dans le noir.
  
# But du jeu 

    - Dans ce jeu nous sommes une chauve souris qui doit aller récuperer des clefs pour passer au niveau supérieur. Elle doit éviter les murs et tirer avec le lightsensor sur les fantomes pour les faire apparaitre. 

# Fonctionnalités

    - Le joueur peut appuyer sur l'écran (onTouch) pour faire accélerer de facon progressive sa chauve souris et la déplacer sur l'axe des abscisses. De la même manière lorsqu'il lâche le bouton il réduit sa vitesse de façon progressive. 
    - Lorsque la luminosité diminue la chauve souris utilise son echolocalisation ( petit laser ). 
    - Pour se déplacer dans l'axe des ordonnées le joueur doit utiliser l'accéléromètre. 
    - L'objectif une fois atteint génère un nouveau niveau 
    - Nous avons décidé de réduire le nombre de niveaux pour atteindre le niveau 3.

# Lien git :
    - https://gitlab.com/Brummbaer/m2dl-mobe-coordinature