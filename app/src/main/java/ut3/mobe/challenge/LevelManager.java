package ut3.mobe.challenge;

import android.util.Pair;

import java.util.ArrayList;

class LevelManager {
	public static final int[] LEVEL_IDENTIFIERS = new int[] {
			R.array.level_1,
			R.array.level_2,
			R.array.level_3,
			R.array.level_4,
			R.array.level_5
	};

	private final MainActivity activity;
	private ArrayList<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> levels;

	public LevelManager(MainActivity activity) {
		this.activity = activity;
	}

	public void loadLevel(String[] level) {
		this.levels = new ArrayList<>();

		for (String data : level) {
			String[] splitData = data.split(",");

			switch (splitData[0]) {
				case "P":
					movePlayer(splitData[1], splitData[2]);

					break;
				case "K":
					moveTarget(splitData[1], splitData[2]);

					break;
				case "W":
					this.levels.add(new Pair<>
							(
									new Pair<>(Integer.valueOf(splitData[1]), Integer.valueOf(splitData[2])),
									new Pair<>(Integer.valueOf(splitData[3]), Integer.valueOf(splitData[4]))
							)
					);

					break;
				case "O":
					createEnemy(splitData[1], splitData[2]);

					break;
				case "Z":
					createGarbageTruck(splitData[1], splitData[2]);
			}
		}

		createWalls();
	}

	private void createGarbageTruck(String x, String y) {
		activity.createGarbageTruck(Integer.parseInt(x), Integer.parseInt(y));
	}

	private void createEnemy(String x, String y) {
		activity.createEnemy(Integer.parseInt(x), Integer.parseInt(y));
	}

	private void movePlayer(String x, String y) {
		activity.movePlayerToPosition(Integer.parseInt(x), Integer.parseInt(y));
	}

	private void moveTarget(String x, String y) {
		activity.moveTarget(
				Integer.parseInt(x),
				Integer.parseInt(y)
		);
	}

	private void createWalls() {
		for (Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> coordinates : this.levels) {
			activity.addWall(
					coordinates.first.first, coordinates.first.second,
					coordinates.second.first, coordinates.second.second
			);
		}
	}
}
