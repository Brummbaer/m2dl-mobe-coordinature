package ut3.mobe.challenge;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;
import android.view.View;

class LightSensorEventListener implements SensorEventListener {
	public static final int LIGHT_THRESHOLD = 40;

	float currentLux = 0;
	MainActivity activity;

	int targetRayY;

	int rayY;
	boolean threadOn = false;

	private Handler laser = new Handler();

	private Runnable laserThread = new Runnable() {
		@Override
		public void run() {
			rayY = (int) activity.ray.getY();
			if (currentLux < LIGHT_THRESHOLD && targetRayY < rayY && threadOn) {
				activity.checkEnemyHit((int) activity.ray.getX(), rayY - 10);

				activity.ray.setY(rayY - 10);

				laser.postDelayed(this, 10);
			} else {
				activity.removeRays();
				threadOn = false;
			}
		}
	};

	public LightSensorEventListener(SensorManager sensorManager, MainActivity mainActivity) {
		Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		this.activity = mainActivity;

		sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
			currentLux = event.values[0];

			Log.d("SensorActivity", String.valueOf(currentLux));

			if (currentLux < LIGHT_THRESHOLD) {
				if (!threadOn) {
					threadOn = true;

					activity.ray.setVisibility(View.VISIBLE);
					activity.ray.setX(activity.playerX + 35);
					activity.ray.setY(activity.playerY - 80);
					targetRayY = (int) activity.ray.getY() - 600;
					laser.postDelayed(laserThread, 100);
				}
			} else {
				threadOn = false;
				activity.removeRays();
			}
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}

	public void setThreadOn(boolean threadOn) {
		this.threadOn = threadOn;
	}
}
