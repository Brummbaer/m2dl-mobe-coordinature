package ut3.mobe.challenge.models;

import android.util.Pair;

public class Enemy extends Obstacle {
	public Enemy(Pair<Integer, Integer> topLeft, Pair<Integer, Integer> topRight, Pair<Integer, Integer> bottomLeft, Pair<Integer, Integer> bottomRight) {
		super(topLeft, topRight, bottomLeft, bottomRight);
	}

	@Override
	public String toString() {
		return "Enemy{" +
				"topLeft=" + topLeft +
				", topRight=" + topRight +
				", bottomLeft=" + bottomLeft +
				", bottomRight=" + bottomRight +
				'}';
	}
}
