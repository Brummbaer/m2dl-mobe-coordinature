package ut3.mobe.challenge.models;

import android.util.Pair;

abstract class Obstacle {
	protected Pair<Integer, Integer> topLeft;
	protected Pair<Integer, Integer> topRight;
	protected Pair<Integer, Integer> bottomLeft;
	protected Pair<Integer, Integer> bottomRight;

	public Obstacle(Pair<Integer, Integer> topLeft, Pair<Integer, Integer> topRight,
					   Pair<Integer, Integer> bottomLeft, Pair<Integer, Integer> bottomRight) {
		this.topLeft = topLeft;
		this.topRight = topRight;
		this.bottomLeft = bottomLeft;
		this.bottomRight = bottomRight;
	}

	public boolean isInBounds(Pair<Integer, Integer>[] points) {
		for (Pair<Integer, Integer> coordinates : points) {
			if (
				/* ### X axis ### */
				coordinates.first >= topLeft.first && coordinates.first <= topRight.first &&
				/* ### Y axis ### */
				coordinates.second >= topLeft.second && coordinates.second <= bottomLeft.second
			) {
				return true;
			}
		}

		if (topLeft.first >= points[0].first && topLeft.first <= points[1].first &&
				topLeft.second >= points[0].second && topLeft.second <= points[2].second) {
			return true;
		}

		if (topRight.first >= points[0].first && topRight.first <= points[1].first &&
				topRight.second >= points[0].second && topRight.second <= points[2].second) {
			return true;
		}

		if (bottomLeft.first >= points[0].first && bottomLeft.first <= points[1].first &&
				bottomLeft.second >= points[0].second && bottomLeft.second <= points[2].second) {
			return true;
		}

		if (bottomRight.first >= points[0].first && bottomRight.first <= points[1].first &&
				bottomRight.second >= points[0].second && bottomRight.second <= points[2].second) {
			return true;
		}


		return false;
	}
}
