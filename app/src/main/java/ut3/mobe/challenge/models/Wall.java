package ut3.mobe.challenge.models;

import android.util.Pair;

public class Wall extends Obstacle {
   public Wall(Pair<Integer, Integer> topLeft, Pair<Integer, Integer> topRight, Pair<Integer, Integer> bottomLeft, Pair<Integer, Integer> bottomRight) {
      super(topLeft, topRight, bottomLeft, bottomRight);
   }

   @Override
   public String toString() {
      return "Wall{" +
              "topLeft=" + topLeft +
              ", topRight=" + topRight +
              ", bottomLeft=" + bottomLeft +
              ", bottomRight=" + bottomRight +
              '}';
   }
}
