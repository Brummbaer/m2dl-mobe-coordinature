package ut3.mobe.challenge;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

class OnTouchEventListener implements View.OnTouchListener {
	private static final int MAXIMUM_ACCELERATION = 4;

	private final GameView game;
	private final MainActivity activity;
	private MotionEvent motionEventOnTouch;
	private Handler accelerationDecay = new Handler();

	private Runnable accelerationDecayThread = new Runnable() {
		@Override
		public void run() {
			int current = activity.getMultiplier();

			if (current > 1) {
				activity.setMultiplier(current - 1);

				accelerationDecay.postDelayed(this, 1000);
			}
		}
	};

	private Handler acceleration = new Handler();

	private Runnable accelerationThread = new Runnable() {
		@Override
		public void run() {
			if(motionEventOnTouch.getAction() != MotionEvent.ACTION_UP) {
				int current = activity.getMultiplier();
				int x = (int) motionEventOnTouch.getX();

				if (x > activity.deviceUtilities.getDeviceWidth() / 2) {
					activity.movePlayer(-1, 0);
				} else {
					activity.movePlayer(1, 0);
				}
				if (current < MAXIMUM_ACCELERATION) {
					activity.setMultiplier(current + 1);
				}
				acceleration.postDelayed(this, 1);
			}
		}
	};


	public OnTouchEventListener(GameView gameView, MainActivity mainActivity) {
		this.game = gameView;
		this.activity = mainActivity;

		this.game.setOnTouchListener(this);
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {
		motionEventOnTouch = motionEvent;
		if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
			acceleration.postDelayed(accelerationThread,100);
		} else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
			accelerationDecay.postDelayed(accelerationDecayThread, 300);
		}

		return true;
	}
}
