package ut3.mobe.challenge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Menu extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.initialize();

		setContentView(R.layout.activity_menu);
	}

	public void initialize() {
		setContentView(R.layout.activity_menu);
	}

	public void onClickHandler(View view) {
		Intent intent = new Intent(this, MainActivity.class);
		this.startActivity(intent);
	}
}