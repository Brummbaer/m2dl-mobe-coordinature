package ut3.mobe.challenge;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import ut3.mobe.challenge.models.Enemy;
import ut3.mobe.challenge.models.Wall;

public class MainActivity extends Activity {
	private ImageView backgroundImage;
	private ImageView player;

	public ImageView ray;
	private ImageView rayLeft;
	private ImageView rayRight;

	private GameView gameView;

	public int playerX;
	public int playerY;

	private int playerWidth;
	private int playerHeight;

	private ImageView key;

	private int multiplier = 1;

	DisplayMetrics displayMetrics;

	private RelativeLayout obstacleLayer;

	private LevelManager levelManager;
	private int currentLevel = 0;

	private ArrayList<Wall> walls = new ArrayList<>();
	private HashMap<Enemy, ImageView> enemies = new HashMap<>();

	DeviceUtilities deviceUtilities;

	private LightSensorEventListener lightSensorEventListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DisplayMetrics displayMetrics = new DisplayMetrics();
		this.displayMetrics = displayMetrics;

		setContentView(R.layout.activity_main);
		initializeInterface();

		deviceUtilities = new DeviceUtilities(displayMetrics);

		playerWidth = player.getDrawable().getIntrinsicWidth();
		playerHeight = player.getDrawable().getIntrinsicHeight();

		levelManager = new LevelManager(this);
		levelManager.loadLevel(getResources().getStringArray(LevelManager.LEVEL_IDENTIFIERS[currentLevel]));

		new OnTouchEventListener(gameView, this);
		new AccelerometerSensorEventListener((SensorManager) getSystemService(SENSOR_SERVICE), this);
		lightSensorEventListener = new LightSensorEventListener((SensorManager) getSystemService(SENSOR_SERVICE), this);

		//gameTimer = new GameTimer(this);
	}

	private void initializeInterface() {
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		backgroundImage = (ImageView) findViewById(R.id.backgroundImage);
		player = (ImageView) findViewById(R.id.player);

		ray = (ImageView) findViewById(R.id.ray);
		rayLeft = (ImageView) findViewById(R.id.rayLeft);
		rayRight = (ImageView) findViewById(R.id.rayRight);

		obstacleLayer = findViewById(R.id.obstacles);

		key = findViewById(R.id.key);

		SurfaceView gameSurface = (SurfaceView) findViewById(R.id.gameSurface);

		ViewGroup rootLayout = (ViewGroup) gameSurface.getParent();

		int gameSurfaceIndex = rootLayout.indexOfChild(gameSurface);

		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

		rootLayout.removeViewAt(gameSurfaceIndex);
		gameView = new GameView(this);
		rootLayout.addView(gameView, gameSurfaceIndex);
	}

	public void createEnemy(int x, int y) {
		Pair<Integer, Integer> coordinates = deviceUtilities.getScaledCoordinatesFromPercentageValues(x, y);

		ImageView enemy = new ImageView(this);
		enemy.setImageResource(R.drawable.ghost);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100, 100);

		params.leftMargin = coordinates.first;
		params.topMargin = coordinates.second;

		obstacleLayer.addView(enemy, params);
		enemy.setVisibility(View.INVISIBLE);

		Enemy e = new Enemy(
				coordinates,
				new Pair<>(coordinates.first + 100, coordinates.second),
				new Pair<>(coordinates.first, coordinates.second + 100),
				new Pair<>(coordinates.first + 100, coordinates.second + 100)
		);

		this.enemies.put(e, enemy);
	}

	public void createGarbageTruck(int x, int y) {
		Pair<Integer, Integer> coordinates = deviceUtilities.getScaledCoordinatesFromPercentageValues(x, y);

		ImageView gt = new ImageView(this);
		gt.setImageResource(R.drawable.car_from_above);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(250, 100);

		params.leftMargin = coordinates.first;
		params.topMargin = coordinates.second;

		obstacleLayer.addView(gt, params);
	}

	public void moveTarget(int x, int y) {
		Pair<Integer, Integer> coordinates = deviceUtilities.getScaledCoordinatesFromPercentageValues(x, y);

		key.setX(coordinates.first);
		key.setY(coordinates.second);
	}

	public void movePlayerToPosition(int x, int y) {
		Pair<Integer, Integer> coordinates = deviceUtilities.getScaledCoordinatesFromPercentageValues(x, y);

		playerX = coordinates.first;
		playerY = coordinates.second;

		this.player.setX(playerX);
		this.player.setY(playerY);
	}

	public void movePlayer(int deltaX, int deltaY) {
		Pair<Integer, Integer>[] playerCoordinates = new Pair[4];

		int playerDestX = playerX - (deltaX * this.multiplier);
		int playerDestY = playerY + (deltaY * this.multiplier);

		// Top left
		playerCoordinates[0] = new Pair<>(playerDestX, playerDestY);

		// Top right
		playerCoordinates[1] = new Pair<>(playerDestX + playerWidth, playerDestY);

		// Bottom left
		playerCoordinates[2] = new Pair<>(playerDestX, playerDestY + playerHeight);

		// Bottom right
		playerCoordinates[3] = new Pair<>(playerDestX + playerWidth, playerDestY + playerHeight);

		for (Wall w : this.walls) {
			if (w.isInBounds(playerCoordinates)) {
				return;
			}
		}

		for (Enemy e : this.enemies.keySet()) {
			if (e.isInBounds(playerCoordinates)) {
				return;
			}
		}

		/* ### X axis ### */
		if ((0 <= playerX) && (deltaX > 0) || (playerX + 100 < deviceUtilities.getDeviceWidth()) && (deltaX < 0)) {
			this.playerX = this.playerX - (deltaX * this.multiplier);
		}

		/* ### Y axis ### */
		if ((0 <= playerY) && (deltaY < 0) || (playerY + 100 < deviceUtilities.getDeviceHeight()) && (deltaY > 0)) {
			this.playerY = this.playerY + (deltaY * this.multiplier);
		}

		player.setX(playerX);
		player.setY(playerY);

		checkIfPlayerOnTarget();
	}

	private void checkIfPlayerOnTarget() {
		if (playerX > key.getX() - key.getWidth() && playerX < key.getX() + key.getWidth() &&
				playerY > key.getY() - key.getHeight() && playerY < key.getY() + key.getHeight()) {
			increaseScore();
		}
	}

	/**
	 * Method called when the player reached the target, and finished the current level.
	 */
	private void increaseScore() {
		Log.d("PlayerWon", "You won! Next level: " + (currentLevel + 1));

		clearObstacles();

		if (currentLevel + 1 < LevelManager.LEVEL_IDENTIFIERS.length) {
			levelManager.loadLevel(getResources().getStringArray(LevelManager.LEVEL_IDENTIFIERS[++currentLevel]));
		}
	}

	private void clearObstacles() {
		obstacleLayer.removeAllViews();

		this.walls.clear();
		this.enemies.clear();
	}

	public void setMultiplier(int value) {
		this.multiplier = value;
	}

	public int getMultiplier() {
		return multiplier;
	}

	public void addWall(Integer beginX, Integer beginY, Integer endX, Integer endY) {
		LinearLayout wall = new LinearLayout(this);
		wall.setOrientation(LinearLayout.HORIZONTAL);
		wall.setBackgroundResource(R.drawable.wall);

		Pair<Integer, Integer> coordinates = deviceUtilities.getScaledCoordinatesFromPercentageValues(beginX, beginY);

		int width = deviceUtilities.getDeviceWidth() * Math.abs(endX - beginX) / 100;
		int height = deviceUtilities.getDeviceHeight() * Math.abs(endY - beginY) / 100;

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				width, height
		);

		params.leftMargin = coordinates.first;
		params.topMargin = coordinates.second;

		this.obstacleLayer.addView(wall, params);

		Wall w = new Wall(
				coordinates,
				new Pair<>(coordinates.first + width, coordinates.second),
				new Pair<>(coordinates.first, coordinates.second + height),
				new Pair<>(coordinates.first + width, coordinates.second + height)
		);
		walls.add(w);

		Log.d("WallGeneration", String.valueOf(w));
	}

	public void removeRays() {
		ray.setVisibility(View.INVISIBLE);
		rayLeft.setVisibility(View.INVISIBLE);
		rayRight.setVisibility(View.INVISIBLE);
	}

	public void checkEnemyHit(int x, int y) {
		for (Enemy e : enemies.keySet()) {
			Pair[] rayCoordinates = {
					new Pair(x, y),
					new Pair(x + ray.getWidth(), y),
					new Pair(x, y + ray.getHeight()),
					new Pair(x + ray.getWidth(), y + ray.getHeight())
			};

			if (e.isInBounds(rayCoordinates)) {
				enemies.get(e).setVisibility(View.VISIBLE);
				ray.setVisibility(View.INVISIBLE);

				lightSensorEventListener.setThreadOn(false);
			}
		}
	}
}