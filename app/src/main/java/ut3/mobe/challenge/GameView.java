package ut3.mobe.challenge;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.view.SurfaceView;

class GameView extends SurfaceView {

	public GameView(Context context) {
		super(context);

		setZOrderOnTop(true);
		getHolder().setFormat(PixelFormat.TRANSPARENT);
	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);

		canvas.drawColor(0, PorterDuff.Mode.CLEAR);
	}
}
