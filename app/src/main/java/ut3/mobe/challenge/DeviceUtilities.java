package ut3.mobe.challenge;

import android.util.DisplayMetrics;
import android.util.Pair;

class DeviceUtilities {
   private final DisplayMetrics metrics;
   private final int deviceHeight;
   private final int deviceWidth;

   public DeviceUtilities(DisplayMetrics displayMetrics) {
      this.metrics = displayMetrics;

      this.deviceHeight = displayMetrics.heightPixels;
      this.deviceWidth = displayMetrics.widthPixels;
   }

   public int getDeviceHeight() {
      return deviceHeight;
   }

   public int getDeviceWidth() {
      return deviceWidth;
   }

   public Pair<Integer, Integer> getScaledCoordinatesFromPercentageValues(int x, int y) {
      int deviceScaledX = deviceWidth * x / 100;
      int deviceScaledY = deviceHeight * y / 100;

      return new Pair<>(deviceScaledX, deviceScaledY);
   }
}
